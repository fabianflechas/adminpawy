import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {  PawyPupsService } from './services/pawy-pups.service';
import { ArrayPipe } from './pipes/array.pipe'


@NgModule({
  declarations: [
    ArrayPipe
  ],
  imports: [
    CommonModule
  ],
  providers: [
    PawyPupsService
  ],
  exports: [
    ArrayPipe
  ],
})
export class CoreModule { }
