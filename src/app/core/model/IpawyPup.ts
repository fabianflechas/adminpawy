export class IpawyPup {
    id!: string;
    photo!: string;
    name!: string;
    gender!: string;
    breed!: string;
    birth!: Date;
    temperament!: string;
    condition!: string;
    location!: string;
    status!: string;
    story!: string;
    interest: any;
}