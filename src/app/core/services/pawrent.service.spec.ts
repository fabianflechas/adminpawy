import { TestBed } from '@angular/core/testing';

import { PawrentService } from './pawrent.service';

describe('PawrentService', () => {
  let service: PawrentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PawrentService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
