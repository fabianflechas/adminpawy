import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { IpawyPup } from '../model/IpawyPup';
import { map } from 'rxjs/operators';
import firebase from 'firebase'; 
import Timestamp = firebase.firestore.Timestamp
import { pawrent } from '../model/pawrent';


@Injectable({
  providedIn: 'root'
})
export class PawyPupsService {
 
  pawysCollection!: AngularFirestoreCollection<IpawyPup>;
  pawys: Observable<IpawyPup[]>;
  pawyDoc!: AngularFirestoreDocument<IpawyPup>;

  constructor(  
    public afs: AngularFirestore
    ) 
    {
    this.pawysCollection = this.afs.collection<IpawyPup>('pawyPups');

    //this.pawys = this.afs.collection<IpawyPup>('pawyPups').valueChanges();
    // order ::: ('items', ref => ref.orderBy('title','asc'));
    
    this.pawys = this.pawysCollection.snapshotChanges().pipe(map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as any;
        Object.keys(data).filter(key => data[key] instanceof Timestamp)
        .forEach(key => data[key] = data[key].toDate())

        data.id = a.payload.doc.id;
        
        return data;
      });
    }));


    }

    getPawyPups() {
      return this.pawys;
    }

    addPaw(pawy: IpawyPup){
      return new Promise((resolve, reject) => { 
        this.pawysCollection.add(pawy)
        .then(
          result => {
            pawy.id = result.id;
            resolve(pawy.id)
         }
        )
        .catch(
          error => {
              reject(error);
          }
      )
      }); 
    }

    deletePaw(pawy: IpawyPup){
      this.pawyDoc = this.afs.doc(`pawyPups/${pawy.id}`);
      this.pawyDoc.delete();
      console.log("boorar2")
    }
  
    updatePaw(pawy: IpawyPup){
      return new Promise((resolve, reject) => { 
      this.pawyDoc = this.afs.doc(`pawyPups/${pawy.id}`);
      this.pawyDoc.update(pawy)
        .then(
          result => {      
            resolve(result)
         }
        )
        .catch(
          error => {
              reject(error);
          }
      )
      }); 

      
    }

    updateState(pawyId: string, userUid: string){

      return new Promise((resolve, reject) => { 
        this.pawyDoc = this.afs.doc(`pawyPups/${pawyId}`);
        this.pawyDoc.update({status:"Adopted", id: userUid})
          .then(
            result => {      
              resolve(result)
           }
          )
          .catch(
            error => {
                reject(error);
            }
        )
        }); 
    }

   

}
