import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { IpawyPup } from '../model/IpawyPup';
import { map } from 'rxjs/operators';
import firebase from 'firebase'; 
import Timestamp = firebase.firestore.Timestamp
import { pawrent } from '../model/pawrent';
import { combineLatest } from 'rxjs'


@Injectable({
  providedIn: 'root'
})
export class PawrentService {

  pawrentsCollection!: AngularFirestoreCollection<pawrent>;
  pawrents: Observable<pawrent[]>;
  pawrentsDoc!: AngularFirestoreDocument<pawrent>;
  parent!: Observable<pawrent[]>;

  constructor(
    public afs: AngularFirestore,

  ) { 
    this.pawrentsCollection = this.afs.collection<pawrent>('users');

    //this.pawys = this.afs.collection<IpawyPup>('pawyPups').valueChanges();
    // order ::: ('items', ref => ref.orderBy('title','asc'));
    
    this.pawrents = this.pawrentsCollection.snapshotChanges().pipe(map(changes => {
      return changes.map(a => {
        const data = a.payload.doc.data() as any;
        Object.keys(data).filter(key => data[key] instanceof Timestamp)
        .forEach(key => data[key] = data[key].toDate())

        data.id = a.payload.doc.id;
        
        return data;
      });
    }));
  }

  getpawrents() {
    
    return this.pawrents;


 }

 getpawrent(userUid: string){

       return new Promise((resolve, reject) => { 
      this.afs.collection("users")
      .doc(userUid)
      .ref
      .get().then(function(doc) {
          if (doc.exists) {
             const pawy = doc.data()
            //console.log("Document data:", doc.data());
            resolve(pawy)

          } else {
              console.log("No such document!");
          }
      }).catch(function(error) {
          console.log("Error getting document:", error);
      });
     
    }); 
   
 }

 AdoptPawy(pawyId: string, userUid: string){

  return new Promise((resolve, reject) => { 
    var pawy = this.afs.collection('users').doc(userUid);  
    
    pawy.update({
      pawy: firebase.firestore.FieldValue.arrayUnion(pawyId)
    })
    .then(
      result => {      
        resolve(result)
     }
    )
    .catch(
      error => {
          reject(error);
      }
  )
  }); 

  // return new Promise((resolve, reject) => { 
  //   this.pawrentsDoc = this.afs.doc(`users/${userUid}`);
  //   this.pawrentsDoc.update({pawy: pawyId})
  //     .then(
  //       result => {      
  //         resolve(result)
  //      }
  //     )
  //     .catch(
  //       error => {
  //           reject(error);
  //       }
  //   )
  //   }); 

}

// interestPaw(pawyId: string){
//   var interest = this.afs.collection('users').doc(userUid);  

//   return   interest.update({
//     interest: firebase.firestore.FieldValue.arrayUnion(pawyId)
//   })
      

    


// }


}