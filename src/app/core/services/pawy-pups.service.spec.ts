import { TestBed } from '@angular/core/testing';

import { PawyPupsService } from './pawy-pups.service';

describe('PawyPupsService', () => {
  let service: PawyPupsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PawyPupsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
