import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { NavComponent } from './components/nav/nav.component';
import { PawyAdoptFormComponent } from './components/pawyAdopt/pawy-adopt-form/pawy-adopt-form.component';
import { PawyAdoptListComponent } from './components/pawyAdopt/pawy-adopt-list/pawy-adopt-list.component';


import { LayoutModule } from '@angular/cdk/layout';
import { MaterialModule } from '../material/material.module';

import {  PawyPupsService } from '../core/services/pawy-pups.service';
import { InterestedComponent } from './components/pawyAdopt/interested/interested.component'
import  { CoreModule } from '../core/core.module'

@NgModule({
  declarations: [NavComponent, PawyAdoptFormComponent, PawyAdoptListComponent, InterestedComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    LayoutModule,
    ReactiveFormsModule,
    MaterialModule,   
    CoreModule
  ],
  providers: [
    PawyPupsService
  ]
})
export class AdminModule { }
