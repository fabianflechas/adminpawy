import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { PawyPupsService } from '../../../../core/services/pawy-pups.service';
import { PawrentService } from '../../../../core/services/pawrent.service';
import { IpawyPup } from '../../../../core/model/IpawyPup';
import { pawrent } from '../../../../core/model/pawrent';
import { formatDate } from '@angular/common';


@Component({
  selector: 'app-interested',
  templateUrl: './interested.component.html',
  styleUrls: ['./interested.component.scss']
})
export class InterestedComponent implements OnInit {
  form!: FormGroup;
  pawypups!: IpawyPup[];
  pawyPup!: IpawyPup;
  create = true;
  titleInvalid = false;
  users: any;
  pawName!: string;
  photo!: string;
  pawrents: pawrent[] = [];
  pawyId!: string;

  pawrentUser!: pawrent;
  pawrentName!: string;
  pawrentform: any;

  

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private storage: AngularFireStorage,
    private pawyPupsService: PawyPupsService,
    private pawrentService: PawrentService,

  ) {
    this.buildForm();
   }

  ngOnInit(): void {
    this.getPawy();
    this.getPawrent();
  }


  getPawy() {
    // procesar el id pasado por parametro en la ruta
    this.route.paramMap.subscribe((params: ParamMap) => {
      // si hay un parametro id, es porque estamos editando un gift
      if (params.get('id') != null) {
        this.create = false;
        this.pawyPupsService.getPawyPups().subscribe((pawys) => {
          this.pawypups = pawys;

          for (var i = 0; i < pawys.length; i++) {
            if (params.get('id') == pawys[i].id) {
              this.users = pawys[i].interest;
              this.pawName = pawys[i].name;
              this.photo = pawys[i].photo;
              this.pawyId = pawys[i].id
            }
          }
        });
      }
    });
   
  }


  getPawrent() {

   // this.pawrent = this.pawrentService.getpawrents().subscribe();

  //  this.pawrentService.getpawrents(this.users);

  //   console.log("parentsss", this.pawrentService.getpawrents(this.users))
  //   console.log(this.users)

  //    for (var J = 0; J < this.users.length; J++) {
  //     console.log("uno",this.users[0])
  //    }

  this.pawrentService.getpawrents().subscribe((pawrent) => {
          
          this.pawrents = pawrent;
           
         
          // console.log("parents", pawrent)
          // console.log("user1",this.users[0]);
          // console.log("userX",this.pawrentService.getpawrent(this.users[0]));
          
              //  let missing = this.pawrent.id.filter((item: any) => this.users.indexOf(item) < 0);
        //  console.log("misi",missing);  

        
        //   for (var i = 0; i < pawrent.length; i++) {
            
        //     if (this.users[0] == pawrent[i].id) {
        //       this.pawrentUser = pawrent[i];
        //       this.pawrentName = pawrent[i].name;
        //       this.pawrentform = pawrent[i].adoptForm;

        //       //this.array.push  = pawrent[i];
        //     }
          

        // }
          console.log("NAMEusuarios", this.pawrents);
        });
  }


  // getPawrents(){
    
  //   const refs = userIds.map(id => this.firestore.doc(`users/${id}`))
  //   this.firestore.getAll(...refs).then(users => console.log(users.map(doc => doc.data())))
  // }


  private buildForm() {
    this.form = this.formBuilder.group({
      id: [],
      name: ['', [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]],
      gender: ['', [Validators.required]],
      breed: ['', [Validators.required]],
      birth: ['', [Validators.required]],
      temperament: ['', [Validators.required]],
      condition: ['', [Validators.required]],
      location: ['', [Validators.required]],
      status: ['', [Validators.required]],
      photo: ['', [Validators.required]],
      story: ['', [Validators.required]],
      interest: [],
    });
  }

  save(event: any) {
    if (this.form.valid) {
      const data = this.form.value;

      if (this.create) {
        this.pawyPupsService
          .addPaw(data)
          .then((succss) => {
            this.router.navigate(['/admin/pawy-adopt']);
          })
          .catch((error) => {
            console.log('error ' + error, '');
          });
      } else {
        //Edit Paw

        this.pawyPupsService
          .updatePaw(data)
          .then((succss) => {
            this.router.navigate(['/admin/pawy-adopt']);
          })
          .catch((error) => {
            console.log('error ' + error, '');
          });
      }
    } else {
      this.form.markAllAsTouched();
    }
  }

  get nameField() {
    return this.form.get('name');
  }

  get photoField() {
    return this.photo;
  }

  get genderField() {
    return this.form.get('gender');
  }

  get statusField() {
    return this.form.get('status');
  }

adopt(pawrenID: string){
  console.log("lle",pawrenID)
  console.log("lle22",this.pawyId)
  this.pawrentService.AdoptPawy(this.pawyId, pawrenID);
  this.pawyPupsService.updateState(this.pawyId, pawrenID);
}



}
