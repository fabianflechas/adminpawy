import { Component, OnInit } from '@angular/core';
import {
  FormControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { PawyPupsService } from '../../../../core/services/pawy-pups.service';
import { IpawyPup } from '../../../../core/model/IpawyPup';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-pawy-adopt-form',
  templateUrl: './pawy-adopt-form.component.html',
  styleUrls: ['./pawy-adopt-form.component.scss'],
})
export class PawyAdoptFormComponent implements OnInit {
  form!: FormGroup;
  pawypups!: IpawyPup[];
  pawyPup!: IpawyPup;
  create = true;
  titleInvalid = false;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private formBuilder: FormBuilder,
    private storage: AngularFireStorage,
    private pawyPupsService: PawyPupsService
  ) {
    this.buildForm();
  }

  ngOnInit(): void {
    this.getPawy();
  }

  getPawy() {
    // procesar el id pasado por parametro en la ruta
    this.route.paramMap.subscribe((params: ParamMap) => {
      // si hay un parametro id, es porque estamos editando un gift
      if (params.get('id') != null) {
        this.create = false;
        this.pawyPupsService.getPawyPups().subscribe((pawys) => {
          this.pawypups = pawys;

          for (var i = 0; i < pawys.length; i++) {
            if (params.get('id') == pawys[i].id) {
              this.form.setValue({
                id: pawys[i].id,
                name: pawys[i].name,
                gender: pawys[i].gender,
                breed: pawys[i].breed,
                birth: pawys[i].birth,
                temperament: pawys[i].temperament,
                condition: pawys[i].condition,
                location: pawys[i].location,
                status: pawys[i].status,
                photo: pawys[i].photo,
                story: pawys[i].story,
                interest: pawys[i].interest,
              });
            }
          }
        });
      }
    });
  }

  private buildForm() {
    this.form = this.formBuilder.group({
      id: [],
      name: ['', [Validators.required, Validators.pattern(/^[a-zA-Z ]+$/)]],
      gender: ['', [Validators.required]],
      breed: ['', [Validators.required]],
      birth: ['', [Validators.required]],
      temperament: ['', [Validators.required]],
      condition: ['', [Validators.required]],
      location: ['', [Validators.required]],
      status: ['', [Validators.required]],
      photo: ['', [Validators.required]],
      story: ['', [Validators.required]],
      interest: []
    });
  }

  save(event: any) {
    if (this.form.valid) {
      const data = this.form.value;

      if (this.create) {
        this.pawyPupsService
          .addPaw(data)
          .then((succss) => {
            this.router.navigate(['/admin/pawy-adopt']);
          })
          .catch((error) => {
            console.log('error ' + error, '');
          });
      } else {
        //Edit Paw

        this.pawyPupsService
          .updatePaw(data)
          .then((succss) => {
            this.router.navigate(['/admin/pawy-adopt']);
          })
          .catch((error) => {
            console.log('error ' + error, '');
          });
      }
    } else {
      this.form.markAllAsTouched();
    }
  }

  get nameField() {
    return this.form.get('name');
  }

  get photoField() {
    return this.form.get('photo');
  }

  get genderField() {
    return this.form.get('gender');
  }

  get statusField() {
    return this.form.get('status');
  }

  uploadFile(event: any) {
    const image = event.target.files[0];
    const name = Date.now();
    const path = 'pawypup/' + name;
    const ref = this.storage.ref(path);
    const task = this.storage.upload(path, image);

    task
      .snapshotChanges()
      .pipe(
        finalize(() => {
          const urlImage$ = ref.getDownloadURL();
          urlImage$.subscribe((url) => {
            console.log(url);
            this.photoField?.setValue(url);
          });
        })
      )
      .subscribe();
  }

  deletePaw(event: any){
    if (this.form.valid) {
      
      const data = this.form.value;
      this.pawyPupsService.deletePaw(data);
      this.router.navigate(['/admin/pawy-adopt']);

    }else {
      this.form.markAllAsTouched();
      
    }
    
  }
}
