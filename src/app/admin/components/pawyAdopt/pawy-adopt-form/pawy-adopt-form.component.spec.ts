import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PawyAdoptFormComponent } from './pawy-adopt-form.component';

describe('PawyAdoptFormComponent', () => {
  let component: PawyAdoptFormComponent;
  let fixture: ComponentFixture<PawyAdoptFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PawyAdoptFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PawyAdoptFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
