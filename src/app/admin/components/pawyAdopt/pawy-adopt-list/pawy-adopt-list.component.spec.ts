import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PawyAdoptListComponent } from './pawy-adopt-list.component';

describe('PawyAdoptListComponent', () => {
  let component: PawyAdoptListComponent;
  let fixture: ComponentFixture<PawyAdoptListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PawyAdoptListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PawyAdoptListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
