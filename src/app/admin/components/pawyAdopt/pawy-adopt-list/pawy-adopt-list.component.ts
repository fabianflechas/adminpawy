import { Component, OnInit, TemplateRef} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { PawyPupsService } from '../../../../core/services/pawy-pups.service';
import { IpawyPup } from '../../../../core/model/IpawyPup';
import { Router } from '@angular/router';


@Component({
  selector: 'app-pawy-adopt-list',
  templateUrl: './pawy-adopt-list.component.html',
  styleUrls: ['./pawy-adopt-list.component.scss']
})
export class PawyAdoptListComponent implements OnInit {

  public form!: FormGroup;

  pawypups!: IpawyPup [];
  
  adoption = "For adoption"
  

  constructor(
    private fb: FormBuilder,
    private pawyPupsService: PawyPupsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.pawyPupsService.getPawyPups().subscribe(pawys=>{
      this.pawypups = pawys;
    });
  }


  edit(pawy: IpawyPup) {
    this.router.navigate(['/admin/pawy-adopt-form', { id: pawy.id }]);
  }

  interested(pawy: IpawyPup) {
    this.router.navigate(['/admin/pawy-adopt-interested', { id: pawy.id }]);
  }


  

}
