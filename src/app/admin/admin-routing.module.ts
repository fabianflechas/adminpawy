import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NavComponent } from './components/nav/nav.component';
import { PawyAdoptListComponent } from './components/pawyAdopt/pawy-adopt-list/pawy-adopt-list.component';
import { PawyAdoptFormComponent } from './components/pawyAdopt/pawy-adopt-form/pawy-adopt-form.component';
import { InterestedComponent } from './components/pawyAdopt/interested/interested.component'

const routes: Routes = [
 {
   path: '',
   component: NavComponent,
   children:[
     {
       path: 'pawy-adopt',
       component: PawyAdoptListComponent
     },
     {
      path: 'pawy-adopt-form',
      component: PawyAdoptFormComponent
     },
     {
      path: 'pawy-adopt-interested',
      component: InterestedComponent
     },
   ]
 }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
