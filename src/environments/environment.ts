// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC2Httf8txUyLDjt8Le-XTTSPApGHFGrnE",
    authDomain: "pawylove-46d15.firebaseapp.com",
    projectId: "pawylove-46d15",
    storageBucket: "pawylove-46d15.appspot.com",
    messagingSenderId: "140389530599",
    appId: "1:140389530599:web:0a480caec40f01ffab5b42",
    measurementId: "G-TBH559R6GG"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
